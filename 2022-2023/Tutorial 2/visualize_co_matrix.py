from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt
import numpy as np
from typing import List, Dict
import os
import simplejson as sj
import umap


def visualize_embeddings(embeddings: np.ndarray,
                         word_annotations: List[str] = None,
                         word_to_idx: Dict[str, int] = None):
    """
    Plots given reduce word embeddings (2D). Users can highlight specific words (word_annotations list).

    :param embeddings: word embedding matrix of shape (words, 2) retrieved via a dimensionality reduction technique.
    :param word_annotations: list of words to be annotated.
    :param word_to_idx: vocabulary map (word -> index) (dict)
    """
    fig, ax = plt.subplots(1, 1, figsize=(15, 12))

    if word_annotations:
        print(f"Annotating words: {word_annotations}")

        word_indexes = []
        for word in word_annotations:
            word_index = word_to_idx[word]
            word_indexes.append(word_index)

        word_indexes = np.array(word_indexes)

        other_embeddings = embeddings[np.setdiff1d(np.arange(embeddings.shape[0]), word_indexes)]
        target_embeddings = embeddings[word_indexes]

        ax.scatter(other_embeddings[:, 0], other_embeddings[:, 1], alpha=0.1, c='blue')
        ax.scatter(target_embeddings[:, 0], target_embeddings[:, 1], alpha=1.0, c='red')
        ax.scatter(target_embeddings[:, 0], target_embeddings[:, 1], alpha=1, facecolors='none', edgecolors='r', s=1000)

        for word, word_index in zip(word_annotations, word_indexes):
            word_x, word_y = embeddings[word_index, 0], embeddings[word_index, 1]
            ax.annotate(word, xy=(word_x, word_y))
    else:
        ax.scatter(embeddings[:, 0], embeddings[:, 1], alpha=0.1, c='blue')

    # We avoid outliers ruining the visualization if they are quite far away
    xmin_quantile = np.quantile(embeddings[:, 0], q=0.01)
    xmax_quantile = np.quantile(embeddings[:, 0], q=0.99)
    ymin_quantile = np.quantile(embeddings[:, 1], q=0.01)
    ymax_quantile = np.quantile(embeddings[:, 1], q=0.99)
    ax.set_xlim(xmin_quantile, xmax_quantile)
    ax.set_ylim(ymin_quantile, ymax_quantile)

    plt.savefig('plot.png')


def reduce_SVD(embeddings):
    svd = TruncatedSVD(n_components=2, random_state=42)
    reduced = svd.fit_transform(embeddings)
    return reduced


def reduce_tSNE(embeddings):
    tsne = TSNE(n_components=2, random_state=42, n_jobs=4, metric='cosine')
    reduced = tsne.fit_transform(embeddings)
    return reduced


def reduce_umap(embeddings):
    umap_emb = umap.UMAP(n_components=2, n_jobs=4, metric='cosine')
    reduced = umap_emb.fit_transform(embeddings)
    return reduced


def run_visualization(method_name, words_list, word_to_idx, co_occurrence_matrix):
    method_name = method_name.lower().strip()
    if method_name == 'svd':
        reduced = reduce_SVD(co_occurrence_matrix)
    elif method_name == 'tsne':
        reduced = reduce_tSNE(co_occurrence_matrix)
    elif method_name == 'umap':
        reduced = reduce_umap(co_occurrence_matrix)
    else:
        raise RuntimeError(f'Invalid method_name! Got: {method_name}')

    visualize_embeddings(reduced, words_list, word_to_idx)


vocab_path = os.path.join(os.getcwd(), 'prebuilt', 'vocab.json')
with open(vocab_path, mode='r') as f:
    word_to_idx = sj.load(f)

# co_occurrence_matrix = np.load(os.path.join(os.getcwd(), 'prebuilt', 'co_matrix.npy'),
#                                allow_pickle=True).item()
# run_visualization('umap', ['good', 'love', 'beautiful'], word_to_idx, co_occurrence_matrix)
# plt.show()

embedding_matrix = np.load(os.path.join(os.getcwd(), 'prebuilt', 'emb_matrix.npy'),
                           allow_pickle=True)
run_visualization('umap', ['good', 'love', 'beautiful'], word_to_idx, embedding_matrix)
plt.show()
