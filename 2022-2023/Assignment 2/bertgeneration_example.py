from transformers import EncoderDecoderModel, AutoTokenizer

model_name = 'prajjwal1/bert-tiny'

bert2bert = EncoderDecoderModel.from_encoder_decoder_pretrained(model_name, model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)

input_ids = tokenizer('hello there how is it going?', return_tensors='pt').input_ids
labels = tokenizer('it is going well', return_tensors='pt').input_ids

loss = bert2bert(input_ids=input_ids, decoder_input_ids=labels, labels=labels).loss
loss.backward()
print(f'{loss}')

bert2bert.eval()
greedy_output = bert2bert.generate(input_ids, decoder_start_token_id=bert2bert.config.decoder.pad_token_id)
print(greedy_output)