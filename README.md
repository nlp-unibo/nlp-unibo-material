# NLP Course Material - AI UNIBO - Prof. Torroni

## General
This repository stores useful material for the NLP Course held by Prof. Torroni at UniBO.
* Tutorials
* Assignments

## TAs

* <a href="mailto:a.galassi@unibo.it">Andrea Galassi</a>
* <a href="mailto:federico.ruggeri6@unibo.it">Federico Ruggeri</a>

## Contact

If you want to know more about our group, please check our <a href="https://site.unibo.it/nlp/en">dedicated website</a>.